<?php

Class Setup_model extends CI_Model {

    public function getAllCampusSetupInfo()
    {
        return $this->db->query("SELECT * FROM sa_campus a 
                                  LEFT JOIN sa_organizations b ON a.ORG_ID = b.ORG_ID
                                  LEFT JOIN m00_lkpdata c ON a.CAMPUS_TYPE = c.LKP_ID")->result();
    }

    public function getCampusSetupInfoById($id)
    {
        return $this->db->query("SELECT * FROM sa_campus a 
                                  LEFT JOIN sa_organizations b ON a.ORG_ID = b.ORG_ID
                                  LEFT JOIN m00_lkpdata c ON a.CAMPUS_TYPE = c.LKP_ID
                                  WHERE a.CAMPUS_ID = $id")->row();
    }

    public function getAllBuildingInfo()
    {
        return $this->db->query("SELECT *, a.ACTIVE_STATUS as BUILDING_ACTIVE_STATUS FROM sa_building a 
                                  LEFT JOIN sa_campus b ON a.CAMPUS_ID = b.CAMPUS_ID
                                  LEFT JOIN m00_lkpdata c ON a.BUILDING_TYPE = c.LKP_ID")->result();
    }

    public function getBuildingInfoById($building_id)
    {
        return $this->db->query("SELECT *, a.ACTIVE_STATUS as BUILDING_ACTIVE_STATUS FROM sa_building a 
                                  LEFT JOIN sa_campus b ON a.CAMPUS_ID = b.CAMPUS_ID
                                  LEFT JOIN m00_lkpdata c ON a.BUILDING_TYPE = c.LKP_ID WHERE a.BUILDING_ID = $building_id")->row();
    }
}