<?php

Class Inventory_model extends CI_Model {
   public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }


    public function getAllItemSetupInfo()
    {
        return $this->db->query("SELECT item.*, cat.CATEGORY_NAME,unit.UNIT_NAME FROM inv_item item
                                  LEFT JOIN inv_item_category cat ON item.ITEM_CAT_ID = cat.ITEM_CAT_ID
                                  LEFT JOIN inv_unit unit ON item.UNIT_ID = unit.UNIT_ID ")->result();
    }

    public function getItemSetupInfoById($id)
    {
        return $this->db->query("SELECT item.*, cat.CATEGORY_NAME,unit.UNIT_NAME FROM inv_item item
                                  LEFT JOIN inv_item_category cat ON item.ITEM_CAT_ID = cat.ITEM_CAT_ID
                                  LEFT JOIN inv_unit unit ON item.UNIT_ID = unit.UNIT_ID WHERE item.ITEM_ID  = $id")->row();
    }

    public function getAllRequisitionSetupInfo()
    {
        return $this->db->query("SELECT reqchd.*, item.ITEM_NAME,unit.UNIT_NAME,u.USERNAME FROM inv_requisition_chd reqchd
                                  LEFT JOIN inv_item item ON reqchd.ITEM_ID = item.ITEM_ID
                                  LEFT JOIN inv_unit unit ON item.UNIT_ID = unit.UNIT_ID
                                  LEFT JOIN sa_users u on reqchd.CREATED_BY = u.USER_ID")->result();
    }

}