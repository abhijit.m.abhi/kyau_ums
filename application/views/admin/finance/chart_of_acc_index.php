<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Finance List</h5>
        <?php // if ($previlages->CREATE == 1) { ?>
        <div class="ibox-tools">
 <span title="Create Chart of Account" class="btn btn-primary btn-xs pull-right openModal"
                                  data-action="finance/chartofAccFormInsert"> Add New </span>
        </div>
        <?php // } ?>
    </div>
    <div class="ibox-content">
<?php

function display_children($parent_id, $level,$type_id) {
    $sql = "SELECT c.AC_NO,
                       c.AC_NO_UD,
                       c.AC_NAME,
                       c.AC_TYPE_NO,
                       Deriv1.count_row
                FROM fn_achead c
                     LEFT OUTER JOIN (SELECT PARANT_AC_NO, COUNT(*) AS count_row
                                      FROM `fn_achead`
                                      GROUP BY PARANT_AC_NO) Deriv1
                        ON c.AC_NO = Deriv1.PARANT_AC_NO
                WHERE c.PARANT_AC_NO = $parent_id  and c.AC_TYPE_NO=$type_id";
    $CI = get_instance();
    $result = $CI->db->query($sql)->result();
    echo "<ul>";
    if (!empty($result)) {
        foreach ($result as $row) {
            if ($row->count_row > 0) {
                echo '<li> ' . $row->AC_NAME ;
                display_children($row->AC_NO, $level + 1,$row->AC_TYPE_NO);
                echo "</li>";
            } else {
                echo '<li>' . $row->AC_NAME ;
            }
        }
    }
    echo "</ul>";
}
?>
 <ul id="tree1">
                <?php
                echo "<ul><li >Chart of Account</li>";

                foreach ($fn_acctype as $row) {
                    # code...
                
                echo '<li id="open">'.$row->AC_TYPE;
                echo display_children(0, 0,$row->AC_TYPE_NO);
                echo "</li>";
                }
                echo "</ul>";
                ?>
            </ul>


        
    </div>
</div>

<div class="container" style="margin-top:30px;">
    <div class="row">
        <div class="col-md-4">
           
        </div>
       
    </div>
</div>

 <script src="<?php echo base_url(); ?>assets/treeview/jquery.treeview.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/treeview/treeview_custom.css" />