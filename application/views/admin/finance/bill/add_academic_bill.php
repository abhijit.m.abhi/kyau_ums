
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Academic Bill Genarate</h5>
    </div>
    <div class="ibox-content">
        <form class="form-horizontal" id="academic_bill_generate" method="post">
            <span class="frmMsg"></span>
            <div class="form-group">
                <label class="col-lg-3 control-label">Academic Session<span class="text-danger">*</span></label>
                <div class="col-lg-3">
                    <select class="form-control" name="SESSION_ID" id="SESSION_ID">
                        <option value="">--Select--</option>
                        <?php
                        foreach ($ins_session as $row):
                        ?>
                        <option value="<?php echo $row->YSESSION_ID ?>"><?php echo $row->SESSION_NAME ?></option>
                        <?php   endforeach;  ?>
                    </select>
                    <span class="validation"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">Program<span class="text-danger">*</span></label>
                <div class="col-lg-5">
                    <select class="form-control" name="PROGRAM_ID" id="PROGRAM_ID">
                        <option value="">--Select--</option>
                        <?php  foreach ($program as $row):  ?>
                        <option  value="<?php echo $row->PROGRAM_ID ?>" ><?php echo $row->PROGRAM_NAME ?></option>
                        <?php  endforeach;?>
                    </select>
                    <span class="validation"></span>
                </div>
                
            </div>
<!--            <div class="form-group">-->
<!--                <div class="col-lg-10">-->
<!--                    <div id="charge_table">-->
<!--                        <table class="table table-bordered">-->
<!--                            <tr>-->
<!--                                <td class="col-md-1 text-center"><input type="checkbox" name="" id="checkAll"> #</td>-->
<!--                                <td class="col-md-3">Title</td>-->
<!--                                <td class="col-md-1 text-center">Rate</td>-->
<!--                            </tr>-->
<!--                        </table>-->
<!--                    </div>-->
<!--                    <span class="validation"></span>-->
<!--                </div>-->
<!--            </div>-->
            <div class="form-group">
                <div class="col-lg-offset-3 col-lg-8">
                    <span class="modal_msg pull-left"></span>
                    <input type="button" data-action="finance/saveAcademicBill" id="academic_bill_generate_btn" class="btn btn-primary btn-sm form_submit" value="Genarate Bill">
                    <span class="loadingImg"></span>
                </div>
            </div>
        </form>
    </div>
</div>
