<div class="block-flat">
    <form class="form-horizontal" id="account_head_form" action="" method="post">
        <?php
        if ($ac_type == 2) {
            ?>
            <input type="hidden" name="BUILDING_ID" class="rowID" value="<?php echo $building->BUILDING_ID ?>"/>
            <?php
        }
        ?>
        <span class="frmMsg"></span>

        <div class="form-group">
            <label class="col-lg-4 control-label">Account Name<span class="text-danger">*</span></label>
            <div class="col-md-6">
              <input type="text" class="form-control" name="AC_NAME">
              <span class="validation"></span>
          </div>
      </div>

      <div class="hr-line-dashed"></div>
      <div class="form-group">
        <label class="col-lg-4 control-label">Account Code<span class="text-danger">*</span></label>
        <div class="col-md-6">
          <input type="text" class="form-control" name="AC_NO_UD">
          <span class="validation"></span>
      </div>
  </div>

  <div class="hr-line-dashed"></div>


  <div class="hr-line-dashed"></div>
  <div class="form-group">
    <label class="col-lg-4 control-label">Parent Account No.<span class="text-danger">*</span></label>
    <div class="col-lg-6">
        <select class="Building_Type_dropdown form-control " name="PARANT_AC_NO"
        id="PARANT_AC_NO"
        data-tags="true" data-placeholder="Select Building Type" data-allow-clear="true">
        <?php
        if ($ac_type == "2"):
            foreach ($building_type as $row):
                ?>
                <option
                value="<?php echo $row->LKP_ID ?>" <?php echo ($building->BUILDING_TYPE == $row->LKP_ID) ? 'selected' : '' ?>><?php echo $row->LKP_NAME; ?></option>
                <?php
            endforeach;
                    else: // if the form action is VIEW
                    ?>
                    <option value="0">--Select--</option>
                    <?php
                    foreach ($fn_achead as $row):
                        ?>
                        <option value="<?php echo $row->AC_NO ?>"><?php echo $row->AC_NAME ?></option>
                        <?php
                    endforeach;
                    endif; ?>
                </select>
                <span class="validation"></span>
            </div>
        </div>
        
        <div class="hr-line-dashed"></div>
        <div class="form-group" id="AC_TYPE_NO_DIV">
            <label class="col-lg-4 control-label">Account Type<span class="text-danger">*</span></label>
            <div class="col-lg-6">
                <select class="Building_Type_dropdown form-control required" name="AC_TYPE_NO"
                id="AC_TYPE_NO"
                data-tags="true" data-placeholder="Select Building Type" data-allow-clear="true">
                <?php
                if ($ac_type == "2"):
                    foreach ($fn_acctype as $row):
                        ?>
                        <option
                        value="<?php echo $row->AC_TYPE_NO ?>" <?php echo ($building->AC_TYPE_NO == $row->AC_TYPE_NO) ? 'selected' : '' ?>><?php echo $row->AC_TYPE; ?></option>
                        <?php
                    endforeach;
                    else: // if the form action is VIEW
                    ?>
                    <option value="">--Select--</option>
                    <?php
                    foreach ($fn_acctype as $row):
                        ?>
                        <option value="<?php echo $row->AC_TYPE_NO ?>"><?php echo $row->AC_TYPE ?></option>
                        <?php
                    endforeach;
                    endif; ?>
                </select>
                <span class="validation"></span>
            </div>
        </div>   
        <div class="hr-line-dashed"></div>

        <div class="form-group">
            <label class="col-lg-4 control-label">Is Transection?</label>

            <div class="col-lg-6">
                
                <label class="control-label">
                    <?php
                    $data = array(
                        'name' => 'TRANS_FLAG',
                        'id' => 'TRANS_FLAG', 
                    );
                    echo form_checkbox($data);
                    ?>
                </label>

            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group">
            <label class="col-lg-4 control-label">Active?</label>

            <div class="col-lg-6">
                <?php
                $BUILDING_ACTIVE_STATUS = ($ac_type == 2) ? $building->ACTIVE_STATUS : '';
                $checked = ($ac_type == 2) ? (($building->ACTIVE_STATUS == '1') ? TRUE : FALSE) : '';
                ?>
                <label class="control-label">
                    <?php
                    $data = array(
                        'name' => 'status',
                        'id' => 'status',
                        'class' => 'checkBoxStatus',
                        'value' => $BUILDING_ACTIVE_STATUS,
                        'checked' => $checked
                    );
                    echo form_checkbox($data);
                    ?>
                </label>

            </div>
        </div>  

        <div class="form-group">
            <div class="col-lg-offset-4 col-lg-8">
                <span class="modal_msg pull-left"></span>
                <?php if ($ac_type == 2) { ?>
                <input type="button" class="btn btn-primary btn-sm form_submit" data-action="finance/updateBuilding"
                data-su-action="setup/buildingById" value="Update">
                <?php } else { ?>
                <input type="button" class="btn btn-primary btn-sm form_submit" data-action="finance/createAccHead"
                data-su-action="finance/chartOfAccount" data-type="list" value="submit">
                <?php
            }
            ?>
            <input type="reset" class="btn btn-default btn-sm" value="Reset">
            <span class="loadingImg"></span>
        </div>
    </div>
</form>
</div>
<div class="hr-line-dashed"></div>
</form>
</div>
<script>
  
     $(document).on('click', '.checkBoxStatus', function () {
        var status = ($(this).is(':checked') ) ? 1 : 0;
        $("#status").val(status);
    });   
    $(document).on('change', '#PARANT_AC_NO', function () {
        var PARANT_AC_NO = $(this).val();
        if(PARANT_AC_NO == 0){
            $("#AC_TYPE_NO_DIV").show();            
        }else{
         $("#AC_TYPE_NO_DIV").hide();
        }
 });
</script>