<table class="table table-bordered">
    <tr class="info">
        <td colspan="7"><b class="text-warning">Payment Details</b></td>
    </tr>
    <tr>
        <th class="col-md-2">Particular</th>
        <th >Total Amount</th>
        <th>Discount Amount</th>
        <th>Net Amount</th>
        <th>Paid Amount</th>
        <th>Due Amount</th>
        <th>Pay Amount</th>
    </tr>

    <input type="hidden" name="studentId"
           value="<?php echo $student_info->STUDENT_ID; ?>">

    <?php foreach ($semester_wise_payment as $row): ?>
        <input type="hidden" name="accountNo[]"
               value="<?php echo $row->AC_NO; ?>">
        <input type="hidden" name="billingChildId[]"
               value="<?php echo $row->BILLING_CHD_ID; ?>">
        <tr>
            <td><?php echo (empty($row->BILLING_MONTH)) ? $row->AC_NAME : $row->AC_NAME . '<br>' . '<span style="font-size: 10px;">' . '<mark>(' . date('M y', strtotime($row->BILLING_MONTH)) . ')</mark>' . '</span>'; ?></td>
            <td class="text-center"><?php echo $row->TOTAL_BILL ?></td>
            <td class="text-center"><?php echo ($row->DISC_AMT == 0) ? '' : $row->DISC_AMT ?></td>
            <td class="text-center"><?php echo ($row->BILL_AMT == 0) ? '' : $row->BILL_AMT ?></td>
            <td class="text-center"><?php echo ($row->PAID_AMT == 0) ? '' : $row->PAID_AMT ?></td>
            <td class="text-center"><?php echo ($row->DUE_AMT == 0) ? '' : $row->DUE_AMT ?></td>
            <td><input type="text" name="payAmount[]"
                       class="form-control text-center"></td>
        </tr>
    <?php endforeach; ?>
</table>