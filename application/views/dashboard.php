    <!-- orris -->
    <link href="<?php echo base_url(); ?>assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    

    <link href='<?php echo base_url();?>assets/calendar/css/fullcalendar.css' rel='stylesheet' /> 
    <!-- Custom css  -->
    <script src='<?php echo base_url();?>assets/calendar/js/moment.min.js'></script>  
    <script src="<?php echo base_url();?>assets/calendar/js/fullcalendar.min.js"></script>
    <script src='<?php echo base_url();?>assets/calendar/js/bootstrap-colorpicker.min.js'></script> 
    <script src='<?php echo base_url();?>assets/calendar/js/main.js'></script>


    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Dashboard</h5>

      </div>
    <div id="event_modal" class="ibox-content">
      <div class="row">
        <div class="col-lg-3">
          <div class="ibox float-e-margins">
            <div class="ibox-title">
              <span class="label label-success pull-right">Current Session</span>
              <h5>Applicant</h5>
            </div>
            <div class="ibox-content" style="padding: 3px !important">
              <ul class="list-group">
                <li class="list-group-item">
                  <span class="badge badge-primary"><?php echo $tot_applicant; ?></span>
                  Total
                </li>
                <li class="list-group-item ">
                  <span class="badge badge-info"><?php echo $tot_applicant_male; ?></span>
                  Male
                </li>
                <li class="list-group-item">
                  <span class="badge badge-danger"><?php echo $tot_applicant_female; ?></span>
                  Female
                </li>

              </ul>

            </div>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="ibox float-e-margins">
            <div class="ibox-title">
              <span class="label label-info pull-right">Applicant</span>
              <h5>Degree wise</h5>
            </div>
            <div class="ibox-content" style="padding: 3px !important">
             <ul class="list-group">
              <li class="list-group-item">
                <span class="badge badge-primary"><?php echo $tot_applicant_bsc; ?></span>
                Undergraduate
              </li>
              <li class="list-group-item ">
                <span class="badge badge-info"><?php echo $tot_applicant_msc; ?></span>
                Graduate
              </li>
              <li class="list-group-item">
                <span class="badge badge-danger"><?php echo $tot_applicant_diploma; ?></span>
                Diploma
              </li>

            </ul>
          </div>
        </div>
      </div>


      <div class="col-lg-3">
        <div class="ibox float-e-margins">
          <div class="ibox-title">
            <span class="label label-success pull-right">Total</span>
            <h5>Student</h5>
          </div>
          <div class="ibox-content" style="padding: 3px !important">
            <ul class="list-group">
              <li class="list-group-item">
                <span class="badge badge-primary"><?php echo $tot_stu; ?></span>
                Total
              </li>
              <li class="list-group-item ">
                <span class="badge badge-info"><?php echo $tot_stu_male; ?></span>
                Male
              </li>
              <li class="list-group-item">
                <span class="badge badge-danger"><?php echo $tot_stu_female; ?></span>
                Female
              </li>

            </ul>

          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="ibox float-e-margins">
          <div class="ibox-title">
            <span class="label label-info pull-right">Student</span>
            <h5>Degree wise</h5>
          </div>
          <div class="ibox-content" style="padding: 3px !important">
           <ul class="list-group">
            <li class="list-group-item">
              <span class="badge badge-primary"><?php echo $tot_stu_bsc; ?></span>
              Undergraduate
            </li>
            <li class="list-group-item ">
              <span class="badge badge-info"><?php echo $tot_stu_msc; ?></span>
              Graduate
            </li>
            <li class="list-group-item">
              <span class="badge badge-danger"><?php echo $tot_stu_diploma; ?></span>
              Diploma
            </li>

          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-8">
      <div class="ibox float-e-margins ">
        <div class="ibox-title info" style="background-color: #d9edf7">

          <b>Events</b>
        </div>
        <div class="ibox-content" style="padding: 3px !important">

          <div id='dashboard_calendar'></div>
        </div>
      </div>
    </div> 
    <div class="col-lg-4">   
     <div class="client-detail">
      <div class="full-height-scroll">

       <table class="table table-striped table-bordered table-hover ">            
         <tbody>
          <tr class="info">
           <th class="col-md-1 text-center">#</th>
           <th class="col-md-7">Program</th>
           <th class="text-center col-md-2">Total Student</th>
         </tr>
         <?php 
         $SL=1;  
         $total_student=0; 
         $program_wise_tot_student=0;   
         foreach($programs  as $program): 
          ?>
          <tr>
           <td class="text-center"><?php echo $SL++; ?></td>
           <td><?php echo $program->PROGRAM_NAME ?></td>
           <td class="text-center">
             <?php

             $program_wise_tot_student=$this->utilities->countRowByAttribute("student_personal_info", array("PROGRAM_ID" => $program->PROGRAM_ID));

             $total_student +=$program_wise_tot_student; 
             echo ($program_wise_tot_student !='')? $program_wise_tot_student : '' ;
             ?>

           </td>
         </tr>
       <?php endforeach; ?>
       <tr>
         <td class="text-right" colspan="2"><b>Total Student</b> </td>

         <td class="text-center label-warning"><b><?php echo $total_student?></b></td>
       </tr>
     </tbody>
   </table>
 </div>
</div>
</div>
</div>

</div>
<div class="clearfix"> </div>
</div>

<script type="text/javascript">
  $(function(){

    var currentDate; // Holds the day clicked when adding a new event
    var currentEvent; // Holds the event object when editing an event

    $('#color').colorpicker(); // Colopicker 
    var base_url='http://localhost/kyau_ums/'; // Here i define the base_url

    // Fullcalendar
    $('#dashboard_calendar').fullCalendar({
      header: {
        left: 'prev, next, today',
        center: 'title',
        right: 'month, basicWeek, basicDay'
      },
        // Get all events stored in database
        eventLimit: true, // allow "more" link when too many events
        events: base_url+'calendar/getEvents',
        selectable: true,
        selectHelper: true,
        editable: false, // Make the event resizable true   
                // Event Mouseover
                eventMouseover: function(calEvent, jsEvent, view){

                 var tooltip = '<div class="tooltipevent" style="width:200px;height:auto;padding:5px;background:#d9edf7;position:absolute;z-index:10001;">' + calEvent.description + '</div>';
                 $("body").append(tooltip);
                 $(this).mouseover(function(e) {
                  $(this).css('z-index', 10000);
                  $('.tooltipevent').fadeIn('500');
                  $('.tooltipevent').fadeTo('10', 1.9);
                }).mousemove(function(e) {
                  $('.tooltipevent').css('top', e.pageY + 10);
                  $('.tooltipevent').css('left', e.pageX + 20);
                });
              },
              eventMouseout: function(calEvent, jsEvent) {
               $(this).css('z-index', 8);
               $('.tooltipevent').remove();
             },
           });
  });
</script>