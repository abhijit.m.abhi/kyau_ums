<div class="block-flat">
    <form class="form-horizontal frmContent" id="inventory" method="post" action="<?php echo base_url('inventory/createRequisition') ?>">
        <?php
        if ($ac_type == 2) {
            ?>
            <input type="hidden" class="rowID" name="txtUnitId" value="<?php echo $unit->UNIT_ID ?>"/>
            <?php
        }
        ?>
    <span class="frmMsg"></span>
    <section>
      <table id="myTable" class=" table order-list">
    <thead>
        <tr>
            <td>Particulars Name</td>
            <td>Requirement</td>             
            <td class="text-center">Action</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="col-sm-5">
                <!-- <input type="text" name="name" class="form-control" /> -->
                 <select class="Item_dropdown form-control required" name="ITEM_NAME[]" id="ITEM_NAME"
                        data-tags="true" data-placeholder="Select Particulars Name" data-allow-clear="true">
                        <option value="">-Select-</option>
                        <?php
                        foreach ($item_info as $row):
                            ?>
                            <option value="<?php echo $row->ITEM_ID ?>"><?php echo $row->ITEM_NAME."       (".$row->UNIT_NAME.")"; ?></option>
                            <?php
                        endforeach;
                   ?>
                </select>
            </td>
            <td class="col-sm-3">
                <input type="text" name="REQUIREMENT[]"  class="form-control"/>
            </td>
            
            <td class="col-sm-3 text-center">
               <input type="button" class="btn btn-xs btn-success  " id="addrow" value="Add" />
            </td>
             
        </tr>
    </tbody>

</table>

         <div class="form-group"> 
            <label  for="" class="col-md-2 control-label pull-left">Remarks</label>
                <div class=" col-md-5">
                     
                  <textarea class="form-control" name="REMARKS"></textarea> 
                </div>
                
        </div>
 
        <div class="form-group">
            <div class="col-lg-offset-5 col-lg-10">
                <span class="modal_msg pull-left"></span>                 
                    <input type="submit" class="btn btn-primary btn-sm " value="submit">
                <span class="loadingImg"></span>
            </div>
        </div>
      </section>


    </form>
</div>
<script>
    $(document).on('click', '.checkBoxStatus', function () {
        var status = ($(this).is(':checked') ) ? 1 : 0;
        $("#status").val(status);
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
    var counter = 0;

    $("#addrow").on("click", function () {
        var newRow = $("<tr>");
        var cols = "";


        cols += '<td><select class="form-control" name="ITEM_NAME[]" id="ITEM_NAME_' + counter + '"  >' +
            '<option value="">-Select-</option>' +
            <?php foreach ($item_info as $row) { ?>
            '<option value="<?php echo $row->ITEM_ID ?>"><?php echo $row->ITEM_NAME."       (".$row->UNIT_NAME.")"; ?></option>' +
            <?php } ?>
            '</select> </td>';
        cols += '<td><input type="text" class="form-control" name="REQUIREMENT[]" id="REQUIREMENT_' + counter + '"/></td>';
  
       

        cols += '<td class="text-center"><input type="button" class="ibtnDel btn btn-xs btn-danger  "  value="Delete"></td>';
        newRow.append(cols);
        $("table.order-list").append(newRow);
        counter++;
    });



    $("table.order-list").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();       
        counter -= 1
    });


});



function calculateRow(row) {
    var price = +row.find('input[name^="price"]').val();

}

function calculateGrandTotal() {
    var grandTotal = 0;
    $("table.order-list").find('input[name^="price"]').each(function () {
        grandTotal += +$(this).val();
    });
    $("#grandtotal").text(grandTotal.toFixed(2));
}
</script>