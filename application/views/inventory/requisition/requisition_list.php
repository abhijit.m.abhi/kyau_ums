<?php if (1) { ?>
<table class="table table-bordered gridTable"  table-title="Requisition List" table-msg="All Requisition list">
    <thead>
        <tr>
            <th>SN</th>
            <th>Particulars Name</th>
            <th>Requirement Quantity</th>
            <th>Requisition By</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php if (!empty($requisition_info)): ?>
            <?php $sn = 1; ?>
            <?php foreach ($requisition_info as $row) { ?>
            <tr class="gradeX" id="row_<?php echo $row->REQ_CHD_ID; ?>">
                <td <?php echo ($row->ACTIVE_STATUS == 1) ? "" : "class='inactive'"; ?>>
                    <span><?php echo $sn++; ?></span><span class="hidden"
                    id="loader_<?php echo $row->REQ_CHD_ID; ?>"></span></td>
                    <td <?php echo ($row->ACTIVE_STATUS == 1) ? "" : "class='inactive'"; ?>><?php echo $row->ITEM_NAME; ?></td>
                    <td <?php echo ($row->ACTIVE_STATUS == 1) ? "" : "class='inactive'"; ?>><?php echo $row->REQUIREMENT_QTY. "       (".$row->UNIT_NAME.")"; ?></td>
                    <td <?php echo ($row->ACTIVE_STATUS == 1) ? "" : "class='inactive'"; ?>><?php echo $row->USERNAME; ?></td>
                    <td <?php echo ($row->ACTIVE_STATUS == 1) ? "" : "class='inactive'"; ?>>
                        <?php if (1) { ?>
                        <a class="label label-default openModal" id="<?php echo $row->REQ_CHD_ID; ?>"
                         title="Update Requisition Information" data-action="inventory/requisitionFormUpdate"
                         data-type="edit"><i class="fa fa-pencil"></i></a>
                         <?php
                     }
                     if (1) {
                        ?>
                        <a class="label label-danger deleteItem" id="<?php echo $row->REQ_CHD_ID; ?>"
                         title="Click For Delete" data-type="delete" data-field="REQ_CHD_ID" data-tbl="inv_requisition_chd"><i
                         class="fa fa-times"></i></a>
                         <?php
                     }

                     if (1) {
                        ?>
                        <a class="itemStatus" id="<?php echo $row->REQ_CHD_ID; ?>"
                         data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="REQ_CHD_ID"
                         data-field="ACTIVE_STATUS" data-tbl="inv_requisition_chd" data-su-url="inventory/requisitionById">
                         <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Inactive">Active</span>' ?>
                     </a>
                     <?php
                 }
                 ?>
             </td>
         </tr>
         <?php } ?>
     <?php endif; ?>
 </tbody>
 
</table>
<?php
} else {
    echo "<div class='alert alert-danger'>You Don't Have Permission To View This Page</div>";
}
?>