<td <?php echo ($group_data->ACT_FG == 1) ? "" : "class='inactive'"; ?>><span><?php echo $sr; ?></span><span
        class="hidden" id="loader_<?php echo $group_data->LKP_ID; ?>"></span></td>
<td <?php echo ($group_data->ACT_FG == 1) ? "" : "class='inactive'"; ?>><?php echo $group_data->LKP_NAME ?></td>
<td <?php echo ($group_data->ACT_FG == 1) ? "" : "class='inactive'"; ?>>
    <span style="cursor:pointer" id="status<?php echo $group_data->LKP_ID ?>" class="status"
          look_up_id="<?php echo $group_data->LKP_ID ?>" data-status="<?php echo $group_data->ACT_FG ?>"
          data-su-url="lookUp/lookUpById"> <?php echo ($group_data->ACT_FG == 1) ? '<span id="toggol_' . $group_data->LKP_ID . '" class="label label-success" title="Click For Inactive" >Inactive</span>' : '<span id="toggol_' . $group_data->LKP_ID . '" class="label label-danger" title="Click For Active" >Active</span>'; ?> </span>
    <a class="label label-default openLookUpModal" id="<?php echo $group_data->LKP_ID; ?>" title="Edit Group Data"
       data-action="LookUp/lookupDataFormUpdate/<?php echo $group_data->GRP_ID; ?>/<?php echo $group_data->LKP_ID; ?>"
       data-type="edit"><i class="fa fa-pencil"></i></a>
    <a class="label label-danger deletelookup" item_id="<?php echo $group_data->LKP_ID; ?>" title="Click For Delete"
       data-type="delete" data-field="LKP_ID" data-tbl="m00_lkpdata"><i class="fa fa-times"></i></a>
</td>