<style>
    .select2-container {
        z-index: 999999;
    }

    .pop-width {
        width: 25% !important;
    }
</style>
<link href="<?php echo base_url(); ?>assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<div class="block-flat">
    <form class="form-horizontal frmContent" id="Leave" method="post">
        <?php
        if ($ac_type == 2) {
            ?>
            <input type="hidden" class="rowID" name="txtLeaveId" value="<?php echo $leave_info->LEAVE_ID; ?>"/>
            <?php
        }
        ?>

<span class="frmMsg"></span>

        <div class="form-group">
            <label class="col-lg-4 control-label">Leave From<span
                class="text-danger">*</span>
            </label>
            <div class="col-md-6">
                <input type="text" name="start_date" class="form-control datepicker required" value="<?php echo ($ac_type == 2) ? date('d-m-Y', strtotime($leave_info->LEAVE_FORM))  : '' ?>">
            <span class="validation"></span>
            </div>
        </div>

        <div class="hr-line-dashed"></div>

        
        <div class="form-group">
        <label class="col-lg-4 control-label">Leave To<span
            class="text-danger">*</span>
        </label>
        <div class="col-md-6">
            <input type="text" name="end_date" class="form-control datepicker required" value="<?php echo ($ac_type == 2) ? date('d-m-Y', strtotime($leave_info->LEAVE_TO))  : '' ?>">
        <span class="validation"></span>
        </div>
    </div>

    <div class="hr-line-dashed"></div>

    <div class="form-group">
            <label class="col-md-4 control-label">Leave Reason<span
        class="text-danger">*</span></label>

            <div class="col-md-6">
                <textarea class="col-md-12 required"
                          name="leave_reason"><?php echo ($ac_type == 2) ? $leave_info->LEAVE_REASON : ''; ?></textarea>
                <span class="validation"></span>
            </div>
    </div>

    <div class="hr-line-dashed"></div>

    <div class="form-group">
            <label class="col-lg-4 control-label">Emergency Contact<span
        class="text-danger">*</span></label>

        <div class="col-lg-6">
            <input type="text" id="emr_contact" name="emr_contact"
                value="<?php echo ($ac_type == 2) ? $leave_info->EMR_CONTACT : '' ?>" class="form-control required"
                placeholder="Emergency Contact">
            <span class="validation"></span>
            <!-- <span class="help-block m-b-none">Example:- </span> -->
        </div>
    </div>
    <div class="hr-line-dashed"></div>

    <div class="form-group">
            <label class="col-md-4 control-label">Address During Leave<span
        class="text-danger">*</span></label>

            <div class="col-md-6">
                <textarea class="col-md-12 required"
                          name="leave_address"><?php echo ($ac_type == 2) ? $leave_info->ADDRESS_DURING_LEAVE : ''; ?></textarea>
                <span class="validation"></span>
            </div>
    </div>

    <div class="hr-line-dashed"></div>

        <!-- <div class="form-group">
            <label class="col-lg-4 control-label">Leave Type<span class="text-danger">*</span></label>
            <div class="col-lg-6">
                <select class="Leave_Type_dropdown form-control required" name="Leave_TYPE_ID" id="Leave_TYPE_ID"
                        data-tags="true" data-placeholder="Select Leave Type" data-allow-clear="true">
                    <?php
                    if ($ac_type == "edit"): // if the form action is EDIT
                        foreach ($Leave_type as $row):
                            ?>
                            <option
                                value="<?php echo $row->LKP_ID ?>" <?php echo ($leave_info->Leave_TYPE == $row->LKP_ID) ? 'selected' : '' ?>><?php echo $row->LKP_NAME; ?></option>
                            <?php
                        endforeach;
                    else: // if the form action is VIEW
                        ?>
                        <option value="">Select Leave Type</option>
                        <?php
                        foreach ($Leave_type as $row):
                            ?>
                            <option value="<?php echo $row->LKP_ID ?>"><?php echo $row->LKP_NAME ?></option>
                            <?php
                        endforeach;
                    endif; ?>
                </select>
                <span class="validation"></span>
            </div>
        </div>

        <div class="hr-line-dashed"></div> -->

        


        <!-- <div class="form-group"><label class="col-md-4 control-label">Active?</label>

            <div class="col-md-6">
                <?php
                $ACTIVE_STATUS = ($ac_type == "edit") ? $leave_info->ACTIVE_STATUS : '';
                $checked = ($ac_type == "edit") ? (($leave_info->ACTIVE_STATUS == '1') ? TRUE : FALSE) : '';
                ?>
                <label class="control-label">
                    <?php
                    $data = array(
                        'name' => 'status',
                        'id' => 'status',
                        'class' => 'checkBoxStatus',
                        'value' => $ACTIVE_STATUS,
                        'checked' => $checked,
                    );
                    echo form_checkbox($data);
                    ?>
                </label>
                <span class="help-block m-b-none">Click on checkbox for active status.</span>
            </div>
        </div>
        <div class="hr-line-dashed"></div> -->
        <div class="form-group">
            <div class="col-lg-offset-4 col-lg-10">
                <?php if ($ac_type == 2) { ?>
                    <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="teacher/updateLeave"
                           data-su-action="teacher/leaveById" value="Update">
                
                <?php } else { ?>
                    <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="teacher/createLeaveRequest"
                           data-su-action="teacher/leaveList" data-type="list" value="Submit">
                    <?php
                }
                ?>
                <input type="reset" class="btn btn-default btn-sm" value="Reset">
                <span class="loadingImg"></span>
            </div>
        </div>

    </form>
</div>
<script src="<?php echo base_url(); ?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script>
    $( function() {
        $( ".datepicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy' ,
            yearRange: "-50:+0",
            autoclose:true,
            startDate: '-0d',
        });
    } );

    $(document).on('click', '#status', function () {
        var status = ($(this).is(':checked') ) ? 1 : 0;
        $("#status").val(status);
    });
</script>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/redactor/redactor.css"/>
<script src="<?php echo base_url(); ?>assets/redactor/redactor.min.js"></script>

<script type="text/javascript">
    $(document).ready(
        function () {
            $('.redactor').redactor();
        }
    );
</script>
<script>
    $(document).on('click', '.checkBoxStatus', function () {
        var status = ($(this).is(':checked')) ? 1 : 0;
        $("#status").val(status);


    });
    $(".select2DropdownMulti").select2({
        tags: true
    });
</script>

